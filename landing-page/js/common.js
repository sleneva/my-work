$(document).ready(function() {
	var controller = new ScrollMagic.Controller();
	
	// scene1
	var ourScene = new ScrollMagic.Scene({
		triggerElement: '.left',
		reverse: false,
		triggerHook: 0.5
	})
	.setClassToggle('.left', 'fade-in')

	.addTo(controller);

	// scene2
	var ourScene2 = new ScrollMagic.Scene({
		triggerElement: '.magic-title',
		reverse: false,
		triggerHook: 0.3
	})
	.setClassToggle('.magic-title', 'fade-in')

	.addTo(controller);

	// scene3
	var ourScene3 = new ScrollMagic.Scene({
		triggerElement: '.magic-title-2',
		reverse: false,
		triggerHook: 0.5
	})
	.setClassToggle('.magic-title-2', 'fade-in')

	.addTo(controller);

	// scene4
	var ourScene4 = new ScrollMagic.Scene({
		triggerElement: '.reviews',
		reverse: false,
		triggerHook: 0.5
	})
	.setClassToggle('.reviews', 'fade-in')

	.addTo(controller);

	// scene5
	var ourScene5 = new ScrollMagic.Scene({
		triggerElement: '.section-five',
		reverse: false,
		triggerHook: 0.5
	})
	.setClassToggle('.section-five', 'fade-in')
	.addTo(controller);
});